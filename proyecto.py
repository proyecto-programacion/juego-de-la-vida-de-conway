#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import random

largo=int(input("Ingrese un numero: "))

i=0
j=0
matriz = []

for i in range (largo): #Inicializar matriz
	matriz.append([0]*largo)

for i in range (largo): #Llenado de matriz
	for j in range (largo):
		matriz[i][j]= random.randint(0,1)

print(end="-"); print("-"*(largo)*2)
for i in range (largo): #Llenado de matriz
	print(end="|")
	for j in range (largo):
		if matriz[i][j] == 1:
			print("X", end="|")
		else:
			print(" ", end="|")
	print()
print(end="-"); print("-"*(largo)*2)

#función para evaluar los vecinos
def evaluar_matriz(largo,matriz):
	for i in range (largo):
		for j in range (largo):
			#esquina superior izquierda
			if i==0 and j==0:
				contador=matriz[i][j+1]+matriz[i+1][j]+matriz[i+1][j+1]
				print(contador)
			#esquina superior derecha
			if i==0 and j==largo-1:
				contador=matriz[i][j-1]+matriz[i+1][j-1]+matriz[i+1][j]
				print(contador)
			#evaluar fila i cunado es 0 excepto las esquinas	
			if i==0 and j!=0 and j!=largo-1:
				contador=matriz[i][j-1]+matriz[i][j+1]+matriz[i+1][j-1]+matriz[i+1][j]+matriz[i+1][j+1]
				print(contador)
			#esquina inferior izquierda
			if i==largo-1 and j==0:
				contador=matriz[i][j+1]+matriz[i-1][j]+matriz[i-1][j+1]
				print(contador)
			#esquina inferior derecha
			if i==largo-1 and j==largo-1:
				contador=matriz[i][j-1]+matriz[i-1][j]+matriz[i-1][j-1]
				print(contador)
			#evaluar la fila i cuando es el limite (largo) excepto las esquinas
			if i==largo-1 and j!=0 and j!=largo-1:
				contador=matriz[i][j-1]+matriz[i][j+1]+matriz[i-1][j-1]+matriz[i-1][j]+matriz[i-1][j+1]
				print(contador)}
			#evaluar la columna cuando j es 0 excepto las esquinas	
			if j==0 and i!=0 and i!=largo-1:
				contador=matriz[i-1][j]+matriz[i-1][j+1]+matriz[i][j+1]+matriz[i+1][j]+matriz[i+1][j+1]
				print(contador)
			#evaluar la columna cuando j es el limite (largo) excepto las esquinas
			if j==largo-1 and i!=0 and i!=largo-1:
				contador=matriz[i-1][j]+matriz[i-1][j-1]+matriz[i][j-1]+matriz[i+1][j]+matriz[i+1][j-1]
				print(contador)
			#evaluar todo el centro de la matriz, todo lo que no sea el limite de la matriz
			if i!=0 and i!=largo-1 and j!=0 and j!=largo-1:
				contador=matriz[i-1][j-1]+matriz[i-1][j]+matriz[i-1][j+1]+matriz[i][j-1]+matriz[i][j+1]+matriz[i+1][j-1]+matriz[i+1][j]+matriz[i+1][j+1]
				print(contador)



n=evaluar_matriz(largo,matriz)
print(evaluar_matriz)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
import random
import time
import os

#holaaa
# Función para crear la matriz y la matriz auxiliar con las células
# vivas y muertas aleatoriamente.
def crear_matriz_aleatoria(largo):
    i = 0
    j = 0
    matriz = []
    matriz_aux = []
    viva = 0
    muerta = 0

    # Ciclo para crear matriz y matriz auxiliar de largo por largo.
    for i in range(largo):
        matriz.append([0]*largo)
        matriz_aux.append([0]*largo)

    # ciclo para llenar las matrices aleatoriamente con 0 y 1.
    for i in range(largo):
        for j in range(largo):
            matriz[i][j] = random.randint(0, 1)
            matriz_aux[i][j] = random.randint(0, 1)
            # Cuando hay un 1 en una posición de la matriz se aumenta
            # uno en la variable viva.
            if matriz[i][j] == 1:
                viva = viva + 1
            # Cuando hay un 0 en una posición de la matriz se aumenta
            # uno en la variable muerta.
            elif matriz[i][j] == 0:
                muerta = muerta + 1

    # Se imprime la cantidad de células vivas y muertas de la matriz.
    print("		JUEGO DE LA VIDA DE CONWAY		")
    print("vivas: ", viva)
    print("Muertas: ", muerta)

    # Ciclo para imprimir la matriz creada aleatoriamente.
    print(end="-")
    print("-"*(largo)*2)
    for i in range(largo):
        print(end="|")
        for j in range(largo):
            if matriz[i][j] == 1:
                print("X", end="|")
            else:
                print(" ", end="|")
        print()
    print(end="-")
    print("-"*(largo)*2)

    # Tiempo para tardar el cambio de matriz.
    time.sleep(2)
    # Limpiar pantalla.
    os.system("clear")
    # Llamar función para evaluar los vecinos de la matriz.
    evaluar_matriz(largo, matriz, matriz_aux)


# Función para evaluar los vecinos y a la vez mutarla.
def evaluar_matriz(largo, matriz, matriz_aux):
    a = True
    # Ciclo que se repite hasta que esten todas las células muertas.
    while a:
        vivas = 0
        muertas = 0
        # Ciclo para recorrer la la matriz e ir aumentado al contador
        # sumando los vecinos de su posición.
        for i in range(largo):
            for j in range(largo):
                contador = 0
                # Evaluar esquina superior izquierda.
                if i == 0 and j == 0:
                    contador = (matriz[i][j+1] + matriz[i+1][j] +
                                matriz[i+1][j+1])
                # Evaluar esquina superior derecha.
                if i == 0 and j == largo-1:
                    contador = (matriz[i][j-1] + matriz[i+1][j-1] +
                                matriz[i+1][j])
                # Evaluar fila 0 excepto las esquinas.
                if i == 0 and j != 0 and j != largo-1:
                    contador = (matriz[i][j-1] + matriz[i][j+1] +
                                matriz[i+1][j-1] + matriz[i+1][j] +
                                matriz[i+1][j+1])
                # Evaluar esquina inferior izquierda.
                if i == largo-1 and j == 0:
                    contador = (matriz[i][j+1] + matriz[i-1][j] +
                                matriz[i-1][j+1])
                # Evaluar esquina inferior derecha.
                if i == largo-1 and j == largo-1:
                    contador = (matriz[i][j-1] + matriz[i-1][j] +
                                matriz[i-1][j-1])
                # Evaluar fila largo-1 excepto las esquinas.
                if i == largo-1 and j != 0 and j != largo-1:
                    contador = (matriz[i][j-1] + matriz[i][j+1] +
                                matriz[i-1][j-1] + matriz[i-1][j] +
                                matriz[i-1][j+1])
                # Evaluar la columna 0 excepto las esquinas.
                if j == 0 and i != 0 and i != largo-1:
                    contador = (matriz[i-1][j] + matriz[i-1][j+1] +
                                matriz[i][j+1] + matriz[i+1][j] +
                                matriz[i+1][j+1])
                # Evaluar la columna largo-1 excepto las esquinas.
                if j == largo-1 and i != 0 and i != largo-1:
                    contador = (matriz[i-1][j] + matriz[i-1][j-1] +
                                matriz[i][j-1] + matriz[i+1][j] +
                                matriz[i+1][j-1])
                # Evaluar todo el centro de la matriz.
                if i != 0 and i != largo-1 and j != 0 and j != largo-1:
                    contador = (matriz[i-1][j-1] + matriz[i-1][j] +
                                matriz[i-1][j+1] + matriz[i][j-1] +
                                matriz[i][j+1] + matriz[i+1][j-1] +
                                matriz[i+1][j] + matriz[i+1][j+1])

                # Se evalúa cuando hay una célula viva.
                if matriz[i][j] == 1:
                    # Si la suma de sus vecinos es igual o menor a 1 la
                    # célula muere por soledad.
                    if contador <= 1:
                        matriz_aux[i][j] = 0
                    # Si la suma de sus vecinos es 2 o 3 la célula vive.
                    elif contador == 2 or contador == 3:
                        matriz_aux[i][j] = 1
                    # Si la suma de sus vecinos es igual o mayor a 4 la
                    # célula muere por sobreexplotación.
                    elif contador >= 4:
                        matriz_aux[i][j] = 0
                # Se evalúa cuando hay una célula muerta.
                if matriz[i][j] == 0:
                    # Si la suma de sus vecinos es 3 la célula vive.
                    if contador == 3:
                        matriz_aux[i][j] = 1
                    # Si la suma de sus vecinos es distinto a 3 la
                    # célula muere.
                    elif contador != 3:
                        matriz_aux[i][j] = 0

        # Ciclo para evaluar la matriz auxiliar y ver cuantas células
        # vivas y muertas hay.
        for i in range(largo):
            for j in range(largo):
                if matriz_aux[i][j] == 1:
                    vivas = vivas + 1
                elif matriz_aux[i][j] == 0:
                    muertas = muertas + 1

        # Se llama la función para imprimir la matriz mutada.
        imprimir_matriz(largo, matriz_aux, vivas, muertas)
        # Limpiar pantalla.
        os.system("clear")

        # Ciclo para darle el valor de la matriz auxiliar a la matriz
        # llamada matriz para volver a evaluarla.
        for i in range(largo):
            for j in range(largo):
                matriz[i][j] = matriz_aux[i][j]

        # Si la cantidad de células vivas es 0 se termina el ciclo.
        if vivas == 0:
            a = False


# Función para imprimir la matriz.
def imprimir_matriz(largo, matriz_aux, vivas, muertas):
    # Se imprime la cantidad de céluas vivas y células muertas.
    print("		JUEGO DE LA VIDA DE CONWAY		")
    print("Vivas:", vivas)
    print("muertas:", muertas)

    # Ciclo para imprimir la matriz.
    print(end="-")
    print("-"*(largo)*2)
    for i in range(largo):
        print(end="|")
        for j in range(largo):
            if matriz_aux[i][j] == 1:
                print("X", end="|")
            else:
                print(" ", end="|")
        print()
    print(end="-")
    print("-"*(largo)*2)

    # Tiempo para tardar el cambio de matriz.
    time.sleep(2)


# Se define el largo de la matriz aleatoriamente.
largo = random.randint(10, 30)
crear_matriz_aleatoria(largo)
